#include "GepOutputReader/CaloCellsReader.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

#include <string>


CaloCellsReader::CaloCellsReader( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  declareProperty("CaloNoiseTool",m_noiseTool,"Tool Handle for noise tool");
}


CaloCellsReader::~CaloCellsReader() {}


StatusCode CaloCellsReader::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //


  // Noise tool
  if(m_noiseTool.retrieve().isFailure()){
    ATH_MSG_ERROR("Unable to find noise tool");
    return StatusCode::FAILURE;
  }
  else {
    ATH_MSG_INFO("Noise tool retrieved");
  }


  // CaloIndentifier
  CHECK( detStore()->retrieve (m_ccIdHelper, "CaloCell_ID") );


  m_tree = new TTree("caloCellsMap","caloCellsMap");

  m_tree->Branch("cells_eta",&cells_eta);
  m_tree->Branch("cells_phi",&cells_phi);
  m_tree->Branch("cells_sinTh",&cells_sinTh);
  m_tree->Branch("cells_cosTh",&cells_cosTh);
  m_tree->Branch("cells_sinPhi",&cells_sinPhi);
  m_tree->Branch("cells_cosPhi",&cells_cosPhi);
  m_tree->Branch("cells_cotTh",&cells_cotTh);
  m_tree->Branch("cells_x",&cells_x);
  m_tree->Branch("cells_y",&cells_y);
  m_tree->Branch("cells_z",&cells_z);

  m_tree->Branch("cells_etaMin",&cells_etaMin);
  m_tree->Branch("cells_etaMax",&cells_etaMax);
  m_tree->Branch("cells_phiMin",&cells_phiMin);
  m_tree->Branch("cells_phiMax",&cells_phiMax);
  m_tree->Branch("cells_etaGranularity",&cells_etaGranularity);
  m_tree->Branch("cells_phiGranularity",&cells_phiGranularity);
  m_tree->Branch("cells_layer",&cells_layer);

  m_tree->Branch("cells_ID",&cells_ID);
  m_tree->Branch("cells_totalNoise",&cells_totalNoise);
  m_tree->Branch("cells_electronicNoise",&cells_electronicNoise);
  m_tree->Branch("cells_detName",&cells_detName);
  m_tree->Branch("cells_sampling",&cells_sampling);
  m_tree->Branch("cells_badcell",&cells_badcell);

  m_tree->Branch("cells_IsEM", &cells_IsEM);
  m_tree->Branch("cells_IsEM_Barrel", &cells_IsEM_Barrel);
  m_tree->Branch("cells_IsEM_EndCap", &cells_IsEM_EndCap);
  m_tree->Branch("cells_IsEM_BarrelPos", &cells_IsEM_BarrelPos);
  m_tree->Branch("cells_IsFCAL", &cells_IsFCAL);
  m_tree->Branch("cells_IsHEC", &cells_IsHEC);
  m_tree->Branch("cells_IsTile", &cells_IsTile);

  //m_tree->Branch("cells_neighbours", &cells_neighbours);

  CHECK( histSvc()->regTree("/STREAM_CALOMAP/caloCellsMap",m_tree) );

  return StatusCode::SUCCESS;
}

StatusCode CaloCellsReader::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  return StatusCode::SUCCESS;
}

StatusCode CaloCellsReader::execute() {

  //
  //Things that happen once at the end of the event loop go here
  //

  return StatusCode::SUCCESS;
}


StatusCode CaloCellsReader::firstExecute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed


  if(m_noiseTool.retrieve().isFailure()){
    ATH_MSG_ERROR("Unable to find noise tool");
    return StatusCode::FAILURE;
  }
  else {
    ATH_MSG_INFO("Noise tool retrieved");
  }

  //-------
  //Event
  const xAOD::EventInfo* eventInfo = 0;
  CHECK( evtStore()->retrieve( eventInfo, "EventInfo" ) );

  //------------
  // cells info
  const CaloCellContainer* cells = 0;
  CHECK( evtStore()->retrieve( cells, "AllCalo") );

  for(auto cell: *cells){

    Float_t totalNoise = m_noiseTool->getNoise(cell,ICalorimeterNoiseTool::TOTALNOISE);
    Float_t electronicNoise = m_noiseTool->getNoise(cell,ICalorimeterNoiseTool::ELECTRONICNOISE);

    Float_t badcell  = cell->badcell();
    Float_t eta    = cell->eta();
    Float_t phi    = cell->phi();
    Float_t sinTh  = cell->sinTh();
    Float_t cosTh  = cell->cosTh();
    Float_t sinPhi = cell->sinPhi();
    Float_t cosPhi = cell->cosPhi();
    Float_t cotTh  = cell->cotTh();
    Float_t x = cell->x();
    Float_t y = cell->y();
    Float_t z = cell->z();

    UInt_t samplingEnum = m_ccIdHelper->calo_sample(cell->ID());

    Bool_t IsEM = m_ccIdHelper->is_em(cell->ID());
    Bool_t IsEM_Barrel=false;
    Bool_t IsEM_EndCap=false;
    Bool_t IsEM_BarrelPos=false;

    if(IsEM){
      IsEM_Barrel=m_ccIdHelper->is_em_barrel(cell->ID());
      if(IsEM_Barrel){
        if(m_ccIdHelper->pos_neg(cell->ID())>0) IsEM_BarrelPos=true;
      }
      IsEM_EndCap=m_ccIdHelper->is_em_endcap(cell->ID());
    }

    Bool_t isFCAL = m_ccIdHelper->is_fcal(cell->ID());
    Bool_t isHEC  = m_ccIdHelper->is_hec(cell->ID());
    Bool_t isTile = m_ccIdHelper->is_tile(cell->ID());

    std::string detName = CaloSampling::getSamplingName(samplingEnum);

    //std::vector<UInt_t> neighbours;
    //CHECK(CaloCellsReader::getNeighbours(cells, cell, neighbours));

    UInt_t id = (cell->ID().get_identifier32()).get_compact();

    const CaloDetDescriptor *elt = cell->caloDDE()->descriptor();
    Int_t layer = cell->caloDDE()->getLayer();

    Float_t deta = elt->deta();
    Float_t dphi = elt->dphi();

    Float_t etamin = eta - (0.5*deta);
    Float_t etamax = eta + (0.5*deta);

    Float_t phimin = phi - (0.5*dphi);
    Float_t phimax = phi + (0.5*dphi);


    cells_eta              .push_back( eta );
    cells_phi              .push_back( phi );
    cells_sinTh            .push_back( sinTh );
    cells_cosTh            .push_back( cosTh );
    cells_sinPhi           .push_back( sinPhi );
    cells_cosPhi           .push_back( cosPhi );
    cells_cotTh            .push_back( cotTh );
    cells_x                .push_back( x );
    cells_y                .push_back( y );
    cells_z                .push_back( z );

    cells_etaMin           .push_back( etamin );
    cells_etaMax           .push_back( etamax );
    cells_phiMin           .push_back( phimin );
    cells_phiMax           .push_back( phimax );
    cells_etaGranularity   .push_back( deta );
    cells_phiGranularity   .push_back( dphi );
    cells_layer            .push_back( layer );

    cells_ID               .push_back( id );
    cells_totalNoise       .push_back( totalNoise);
    cells_electronicNoise  .push_back( electronicNoise );
    cells_detName          .push_back( detName );
    cells_sampling         .push_back( samplingEnum );
    cells_badcell          .push_back( badcell );

    cells_IsEM             .push_back( IsEM );
    cells_IsEM_Barrel      .push_back( IsEM_Barrel );
    cells_IsEM_EndCap      .push_back( IsEM_EndCap );
    cells_IsEM_BarrelPos   .push_back( IsEM_BarrelPos );
    cells_IsFCAL           .push_back( isFCAL );
    cells_IsHEC            .push_back( isHEC );
    cells_IsTile           .push_back( isTile );

    //cells_neighbours       .push_back( neighbours );

  }


  m_tree->Fill();


  cells_eta              .clear();
  cells_phi              .clear();
  cells_sinTh            .clear();
  cells_cosTh            .clear();
  cells_sinPhi           .clear();
  cells_cosPhi           .clear();
  cells_cotTh            .clear();
  cells_x                .clear();
  cells_y                .clear();
  cells_z                .clear();

  cells_etaGranularity   .clear();
  cells_phiGranularity   .clear();
  cells_etaMin           .clear();
  cells_etaMax           .clear();
  cells_phiMin           .clear();
  cells_phiMax           .clear();
  cells_layer            .clear();

  cells_ID               .clear();
  cells_totalNoise       .clear();
  cells_electronicNoise  .clear();
  cells_detName          .clear();
  cells_sampling         .clear();
  cells_badcell          .clear();

  cells_IsEM             .clear();
  cells_IsEM_Barrel      .clear();
  cells_IsEM_EndCap      .clear();
  cells_IsEM_BarrelPos   .clear();
  cells_IsFCAL           .clear();
  cells_IsHEC            .clear();
  cells_IsTile           .clear();
  //cells_neighbours       .clear();



  //-------


  setFilterPassed(true);
  return StatusCode::SUCCESS;
}





// get neighbours of a given calo cell
StatusCode CaloCellsReader::getNeighbours(const CaloCellContainer* allcells, const CaloCell* acell, std::vector<UInt_t> neighbours){

  const CaloDetDescrManager* m_calo_dd_man  = CaloDetDescrManager::instance();
  const CaloCell_ID* m_calo_id = m_calo_dd_man->getCaloCell_ID();

 // get all neighboring cells
 std::vector<IdentifierHash> cellNeighbours;
 IdentifierHash cellHashID = m_calo_id->calo_cell_hash(acell->ID());
 m_calo_id->get_neighbours(cellHashID,LArNeighbours::super3D,cellNeighbours);

 std::vector<UInt_t> neighbour_ids;
 for (UInt_t iNeighbour = 0; iNeighbour < cellNeighbours.size(); ++iNeighbour) {
   const CaloCell* neighbour = allcells->findCell(cellNeighbours[iNeighbour]);
   if (neighbour) neighbour_ids.push_back((neighbour->ID().get_identifier32()).get_compact());
   else std::cout << "Couldn't access neighbour #" << iNeighbour << " for cell ID " << (acell->ID().get_identifier32()).get_compact() << std::endl;
 }

 neighbours = neighbour_ids;
 return StatusCode::SUCCESS;
}





