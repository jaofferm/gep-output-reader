#include "GepOutputReader/WriteOutputHist.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetContainer.h"
#include "JetEDM/PseudoJetVector.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

#include "CaloEvent/CaloClusterMoment.h"
#include "CaloEvent/CaloClusterMomentStore.h"

using jet::PseudoJetVector;
using xAOD::Jet;

#include <string>

WriteOutputHist::WriteOutputHist(const std::string &name, ISvcLocator *pSvcLocator) : AthAnalysisAlgorithm(name, pSvcLocator)
{

  declareProperty("ClustersList", ClustList);
  declareProperty("JetsList", JetList);
  declareProperty("GetJetConstituentsInfo", m_getJetConstituentsInfo);
  declareProperty("GetJetSeedsInfo", m_getJetSeedsInfo);
}

WriteOutputHist::~WriteOutputHist() {}

StatusCode WriteOutputHist::initialize()
{
  ATH_MSG_INFO("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //


  // Clusters
  if (!ClustList.empty()) {
    for (const auto &cl_name : ClustList) {
      std::cout << "Clust name = " << cl_name << std::endl;
      h_cluster_et[cl_name] = new TH1D(("h_" + cl_name + "_et").c_str(), "", 100, 0, 20000);
      h_cluster_eta[cl_name] = new TH1D(("h_" + cl_name + "_eta").c_str(), "", 100, -4, 4);
      h_cluster_phi[cl_name] = new TH1D(("h_" + cl_name + "_phi").c_str(), "", 100, -3.5, 3.5);
      h_cluster_Nclusters[cl_name] = new TH1D(("h_" + cl_name + "_N").c_str(), "", 2500, 0, 2500);

      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_et").c_str(), h_cluster_et[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_eta").c_str(), h_cluster_eta[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_phi").c_str(), h_cluster_phi[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_N").c_str(), h_cluster_Nclusters[cl_name]));

      if (cl_name.find("Calo") != std::string::npos) {
        h_cluster_Ncells[cl_name] = new TH1D(("h_" + cl_name + "_Ncells").c_str(), "", 3000, 0, 3000);
        CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_Ncells").c_str(), h_cluster_Ncells[cl_name]));
      }
    }
  }


  std :: cout << "m_getJetConstituentsInfo = " << m_getJetConstituentsInfo << std::endl;
  // Jets
  if (!JetList.empty()) {
    for (const auto &jet_name : JetList) {
      h_jet_pt[jet_name] = new TH1D(("h_" + jet_name + "_pt").c_str(), "", 100, 0, 15.e4);
      h_jet_eta[jet_name] = new TH1D(("h_" + jet_name + "_eta").c_str(), "", 100, -4, 4);
      h_jet_phi[jet_name] = new TH1D(("h_" + jet_name + "_phi").c_str(), "", 100, -3.5, 3.5);
      h_jet_m[jet_name] = new TH1D(("h_" + jet_name + "_m").c_str(), "", 100, 0, 15.e4);
      h_jet_Njets[jet_name] = new TH1D(("h_" + jet_name + "_N").c_str(), "", 200, 0, 200);
      h_jet_nConstituents[jet_name] = new TH1D(("h_" + jet_name + "_nConstituents").c_str(), "", 150, 0, 150);

      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_pt").c_str(), h_jet_pt[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_eta").c_str(), h_jet_eta[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_phi").c_str(), h_jet_phi[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_m").c_str(), h_jet_m[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_N").c_str(), h_jet_Njets[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_nConstituents").c_str(), h_jet_nConstituents[jet_name]));

      if (m_getJetConstituentsInfo) {
	      h_jetConst_et[jet_name] = new TH1D(("h_" + jet_name + "_constituentEt").c_str(), "", 100, 0, 10.e4);
	      h_jetConst_eta[jet_name] = new TH1D(("h_" + jet_name + "_constituentEta").c_str(), "", 100, -4, 4);
	      h_jetConst_phi[jet_name] = new TH1D(("h_" + jet_name + "_constituentPhi").c_str(), "", 100, -3.5, 3.5);

	      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_constituentEt").c_str(), h_jetConst_et[jet_name]));
	      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_constituentEta").c_str(), h_jetConst_eta[jet_name]));
	      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_constituentPhi").c_str(), h_jetConst_phi[jet_name]));
      }
      if (jet_name.find("Cone") != std::string::npos)
      {
        if (m_getJetSeedsInfo)
        {
          h_jetSeed_et[jet_name] = new TH1D(("h_" + jet_name + "_seedEt").c_str(), "", 100, 0, 15.e4);
          h_jetSeed_eta[jet_name] = new TH1D(("h_" + jet_name + "_seedEta").c_str(), "", 100, -4, 4);
          h_jetSeed_phi[jet_name] = new TH1D(("h_" + jet_name + "_seedPhi").c_str(), "", 100, -3.5, 3.5);

          CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_seedEt").c_str(), h_jetSeed_et[jet_name]));
          CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_seedEta").c_str(), h_jetSeed_eta[jet_name]));
          CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_seedPhi").c_str(), h_jetSeed_phi[jet_name]));
        }
      }
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputHist::finalize()
{
  ATH_MSG_INFO("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputHist::execute()
{
  ATH_MSG_DEBUG("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  //--------
  // Clusters

  for (const auto &cl_name : ClustList)
  {
    const xAOD::CaloClusterContainer *clusters = nullptr;
    CHECK(evtStore()->retrieve(clusters, cl_name));
    ATH_MSG_DEBUG("Retrieved " + cl_name);

    h_cluster_Nclusters[cl_name]->Fill(clusters->size());

    for (auto iClust : *clusters)
    {
      h_cluster_et[cl_name]->Fill(iClust->et());
      h_cluster_eta[cl_name]->Fill(iClust->eta());
      h_cluster_phi[cl_name]->Fill(iClust->phi());

      // Ncells
      // custom clusters don't have cells info
      if (cl_name.find("Calo4") != std::string::npos)
      {
        CaloClusterCellLink::const_iterator cellBegin = iClust->cell_begin();
        CaloClusterCellLink::const_iterator cellEnd = iClust->cell_end();
        Int_t nCells = std::distance(cellBegin, cellEnd);
        h_cluster_Ncells[cl_name]->Fill(nCells);
      }
    }
  }

  //----------
  // Jets

  for (const auto &jet_name : JetList)
  {
    const xAOD::JetContainer *jets = nullptr;
    CHECK(evtStore()->retrieve(jets, jet_name));
    ATH_MSG_DEBUG("Retrieved " + jet_name);

    h_jet_Njets[jet_name]->Fill(jets->size());

    for (auto iJet : *jets)
    {
      h_jet_pt[jet_name]->Fill(iJet->pt());
      h_jet_eta[jet_name]->Fill(iJet->eta());
      h_jet_phi[jet_name]->Fill(iJet->phi());
      h_jet_m[jet_name]->Fill(iJet->m());
      h_jet_nConstituents[jet_name]->Fill(iJet->numConstituents());

      if (m_getJetConstituentsInfo)
      {
        Int_t c{0};
        const xAOD::JetConstituentVector constvec = iJet->getConstituents();
        for (xAOD::JetConstituentVector::iterator it = constvec.begin(); it != constvec.end(); it++)
        {
          const xAOD::CaloCluster *cl = static_cast<const xAOD::CaloCluster *>((*it)->rawConstituent());
          h_jetConst_et[jet_name]->Fill(cl->et());
          h_jetConst_eta[jet_name]->Fill(cl->eta());
          h_jetConst_phi[jet_name]->Fill(cl->phi());
          c++;
        }
      }

      if (jet_name.find("Cone") != std::string::npos)
      {
        if (m_getJetSeedsInfo)
        {
          h_jetSeed_et[jet_name]->Fill(iJet->getAttribute<Float_t>("SeedEt"));
          h_jetSeed_eta[jet_name]->Fill(iJet->getAttribute<Float_t>("SeedEta"));
          h_jetSeed_phi[jet_name]->Fill(iJet->getAttribute<Float_t>("SeedPhi"));
          std::cout << "RCut = " << iJet->getAttribute<Float_t>("RCut");
        }
      }
    }
  }

  //-------

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}
