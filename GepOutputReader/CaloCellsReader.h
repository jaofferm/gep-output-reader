#ifndef CALOCELLSREADER_CALOCELLSREADERALG_H
#define CALOCELLSREADER_CALOCELLSREADERALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "CaloInterface/ICalorimeterNoiseTool.h"
#include "CaloDetDescr/CaloDetDescrManager.h"
#include "CaloIdentifier/CaloCell_ID.h"

class CaloCellsReader: public ::AthAnalysisAlgorithm {
  public:
    CaloCellsReader( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~CaloCellsReader();

    ///uncomment and implement methods as required
                                          //IS EXECUTED:
    virtual StatusCode  initialize();     //once, before any input is loaded
    //virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
    virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
    virtual StatusCode  execute();        //per event
    //virtual StatusCode  endInputFile();   //end of each input file
    //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
    virtual StatusCode  finalize();       //once, after all events processed

    virtual StatusCode getNeighbours(const CaloCellContainer* allcells, const CaloCell* acell, std::vector<UInt_t> neighbours);

  private:
    ToolHandle<ICalorimeterNoiseTool> m_noiseTool;
    const CaloCell_ID* m_ccIdHelper;

    TTree* m_tree = 0;

    std::vector<Float_t> cells_eta;
    std::vector<Float_t> cells_phi;
    std::vector<Float_t> cells_sinTh;
    std::vector<Float_t> cells_cosTh;
    std::vector<Float_t> cells_cosPhi;
    std::vector<Float_t> cells_sinPhi;
    std::vector<Float_t> cells_cotTh;
    std::vector<Float_t> cells_x;
    std::vector<Float_t> cells_y;
    std::vector<Float_t> cells_z;

    std::vector<Float_t> cells_etaMin;
    std::vector<Float_t> cells_etaMax;
    std::vector<Float_t> cells_phiMin;
    std::vector<Float_t> cells_phiMax;
    std::vector<Float_t> cells_etaGranularity;
    std::vector<Float_t> cells_phiGranularity;
    std::vector<Int_t>   cells_layer;

    std::vector<UInt_t> cells_ID;
    std::vector<Float_t> cells_totalNoise;
    std::vector<Float_t> cells_electronicNoise;
    std::vector<std::string> cells_detName;
    std::vector<UInt_t> cells_sampling;
    std::vector<Bool_t> cells_badcell;

    std::vector<Bool_t> cells_IsEM;
    std::vector<Bool_t> cells_IsEM_Barrel;
    std::vector<Bool_t> cells_IsEM_EndCap;
    std::vector<Bool_t> cells_IsEM_BarrelPos;
    std::vector<Bool_t> cells_IsEM_BarrelNeg;
    std::vector<Bool_t> cells_IsFCAL;
    std::vector<Bool_t> cells_IsHEC;
    std::vector<Bool_t> cells_IsTile;

    //std::vector<std::vector<UInt_t>> neighbours;
};

#endif //> !CALOCELLSREADER_CALOCELLSREADERALG_H
